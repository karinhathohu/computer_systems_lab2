﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPUDemonstration
{
    class Program
    {
        //Writing in binary function overloads
        static public string WriteInBinary(long n)
        {
            //char[] b = new char[64];
            //int pos = 63;
            //int i = 0;

            //while (i < 64)
            //{
            //    if ((n & (1 << i)) != 0)
            //    {
            //        b[pos] = '1';
            //    }
            //    else
            //    {
            //        b[pos] = '0';
            //    }
            //    pos--;
            //    i++;
            //}
            //return new string(b);
            string result = Convert.ToString(n, 2);
            return result;
        }
        static public string WriteInBinary(uint n)
        {
            //char[] b = new char[32];
            //int pos = 31;
            //int i = 0;

            //while (i < 32)
            //{
            //    if ((n & (1 << i)) != 0)
            //    {
            //        b[pos] = '1';
            //    }
            //    else
            //    {
            //        b[pos] = '0';
            //    }
            //    pos--;
            //    i++;
            //}
            //return new string(b);
            string result = Convert.ToString(n,2);
            return result;
        }
        static public string WriteInBinary(ulong n)
        {
            //char[] b = new char[64];
            //int pos = 63;
            //int i = 0;
            //ulong welp;
            //while (i < 64)
            //{
            //    welp = Convert.ToUInt64(Convert.ToUInt64(1) << i);
            //    if ((n & welp) != 0)
            //    {
            //        b[pos] = '1';
            //    }
            //    else
            //    {
            //        b[pos] = '0';
            //    }
            //    pos--;
            //    i++;
            //}
            //return new string(b);
            string result = Convert.ToString((long)n, 2);
            return result;
        }
        static public string WriteInBinary(int n)
        {
            //char[] b = new char[32];
            //int pos = 31;
            //int i = 0;

            //while (i < 32)
            //{
            //    if ((n & (1 << i)) != 0)
            //    {
            //        b[pos] = '1';
            //    }
            //    else
            //    {
            //        b[pos] = '0';
            //    }
            //    pos--;
            //    i++;
            //}
            //return new string(b);
            string result = Convert.ToString(n, 2);
            return result;
        }
        
        //1. ALU Multiplication As-Is
        static public void MultiplyAsIs()
        {
            ulong product = 0;
            ulong multiplicand = 0;
            uint multiplier = 0;
            Console.WriteLine("You wan't to mltiply (1) by (2).\nEnter (1):");

            bool ok = false;
            while (!ok)
            {
                if ((ulong.TryParse(Console.ReadLine(), out multiplicand)) && (multiplicand < Math.Pow(2, 32) - 1))
                {
                    ok = true;
                }
                else
                    Console.WriteLine("Out of 32 bit range, try again.");
            }
            ok = false;
            Console.WriteLine("Enter (2):");
            while (!ok)
            {
                if ((uint.TryParse(Console.ReadLine(), out multiplier)) && (multiplier < Math.Pow(2, 32) - 1))
                    ok = true;
                else
                    Console.WriteLine("Out of 32 bit range, try again.");
            }
            Console.WriteLine("Product register initialized to {0}\nMultiplicand initialized to {1}\nMultiplier initialized to {2}", WriteInBinary(product), WriteInBinary(multiplicand), WriteInBinary(multiplier));

            for (int i = 0; i < 32; i++)
            {
                if (multiplier % 2 == 1)
                {
                    product += multiplicand;
                    Console.WriteLine("multiplier's last bit is 1, \nALU adds product and multiplicand to product.\nProduct is {0}", WriteInBinary(product));
                }
                multiplicand = multiplicand << 1;
                Console.WriteLine("multiplicand is shifted left -> {0}",WriteInBinary(multiplicand));
                multiplier = multiplier >> 1;
                Console.WriteLine("Multiplier is shifted right -> {0}", WriteInBinary(multiplier));
            }

            Console.WriteLine("Resulting product is {0} --- {1}",product, WriteInBinary(product));
        }
        
        //2. ALU Division, from as-is to remainder and quotient in one register method.
        static public void DivideAsIs()
        {
            //divident / divisor = quotient( + remainder)
            //divident - divisor
            long divisor = 2;
            divisor = divisor << 32;
            long remainder = 7; //remainder is divident in the beginning
            int quotient = 0;

            for (int i = 0; i < 32 + 1; i++)
            {
                remainder = remainder - divisor;
                if (remainder >= 0)
                {
                    quotient = quotient << 1;
                    Console.WriteLine("1 q\n{0}\n->", WriteInBinary(quotient));
                    quotient = quotient | 1;
                    Console.WriteLine("q\n{0}", WriteInBinary(quotient));
                }
                else
                {
                    remainder += divisor;
                    quotient = quotient << 1;
                    Console.WriteLine("2 q\n{0}\n->", WriteInBinary(quotient));
                    quotient = quotient & ((int)-2);
                    Console.WriteLine("q\n{0}", WriteInBinary(quotient));
                }
                divisor = divisor >> 1;
            }
            Console.WriteLine("remainder\n{0}\n{1}", WriteInBinary(remainder), remainder);
            Console.WriteLine("quotient\n{0}\n{1}", WriteInBinary(quotient), quotient);
        }
        static public void Dividevariant2() 
        {
            Console.WriteLine("{0}\n{1}\n{2}", WriteInBinary((long)(Math.Pow(2, 32) - 1)), (Math.Pow(2, 32) - 1), (long)(Math.Pow(2, 32) - 1));
            
            int divisor = 33;
            long remainder = 100; //remainder contains  divident in the beginning
            int quotient = 0;
            
            for (int i = 0; i < 32; i++)
            {
                remainder = remainder << 1;
                Console.WriteLine("this is remainder {0} :: {1}", WriteInBinary(remainder), remainder); 
                remainder = ((remainder - ((long)divisor << 32)) & (-4294967296)) + (remainder & (long)4294967295);
                Console.WriteLine("this is remainder - divisor {0} :: {1}", WriteInBinary(remainder), remainder);


                if (remainder >= 0)
                {
                    quotient = quotient << 1;
                    Console.WriteLine("1 q\n{0}\n->", WriteInBinary(quotient));
                    quotient = quotient | 1;
                    Console.WriteLine("q\n{0}", WriteInBinary(quotient));
                }
                else
                {
                    remainder += (long)divisor << 32;
                    quotient = quotient << 1;
                    Console.WriteLine("2 q\n{0}\n->", WriteInBinary(quotient));
                    quotient = quotient & ((int)-2);
                    Console.WriteLine("q\n{0}", WriteInBinary(quotient));
                }               
                
                Console.WriteLine("this is remainder {0} :: {1}", WriteInBinary(remainder), remainder);
            }

            Console.WriteLine("remainder\n{0}\n{1}", WriteInBinary(remainder >> 32), remainder >> 32);
            Console.WriteLine("quotient\n{0}\n{1}", WriteInBinary(quotient), quotient);
        }        
        static public void Dividevariant3() 
        {
            //Console.WriteLine("{0}\n{1}\n{2}", WriteInBinary((long)(Math.Pow(2, 32) - 1)), (Math.Pow(2, 32) - 1), (long)(Math.Pow(2, 32) - 1));

            int divisor = 25;
            long remainder_quotient = 100;
            
            try
            {
                Console.WriteLine("Enter your divident: ");
                remainder_quotient = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter your divisor: ");
                divisor =  int.Parse(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("nope, not this numbers. buy.");
                return;
            }

            Console.WriteLine("Press any key to coninue.");
            Console.ReadKey();

            remainder_quotient = remainder_quotient << 1;
            for (int i = 0; i < 32; i++)
            {
                remainder_quotient = remainder_quotient << 1;
                Console.WriteLine("this is remainder {0} :: {1}", WriteInBinary(remainder_quotient), remainder_quotient);
                remainder_quotient = ((remainder_quotient - ((long)divisor << 32)) & (-4294967296)) + (remainder_quotient & (long)4294967295);
                Console.WriteLine("this is remainder - divisor {0} :: {1}", WriteInBinary(remainder_quotient), remainder_quotient);


                if (remainder_quotient >= 0)
                {
                    Console.WriteLine("1 q\n{0}\n->", WriteInBinary(remainder_quotient));
                    remainder_quotient = remainder_quotient | 1;
                    Console.WriteLine("q\n{0}", WriteInBinary(remainder_quotient));
                }
                else
                {
                    remainder_quotient = ((remainder_quotient + ((long)divisor << 32)) & (-4294967296)) + (remainder_quotient & (long)4294967295);
                    Console.WriteLine("2 q\n{0}\n->", WriteInBinary(remainder_quotient));
                    remainder_quotient = remainder_quotient & ((long)-2);
                    Console.WriteLine("q\n{0}", WriteInBinary(remainder_quotient));
                }

                Console.WriteLine("this is remainder {0} :: {1}", WriteInBinary(remainder_quotient), remainder_quotient);
            }

            remainder_quotient = remainder_quotient >> 1;

            Console.WriteLine("remainder_qoutient\n{0}\n{1}", WriteInBinary(remainder_quotient), remainder_quotient);
            Console.WriteLine("remainder\n{0}\n{1}", WriteInBinary(remainder_quotient >> 32), remainder_quotient >> 32);
            Console.WriteLine("quotient\n{0}\n{1}", WriteInBinary((remainder_quotient << 32) >> 32), (remainder_quotient << 32) >> 32);
        }

        static public int TurnBinFlopToDec()
        {

            return 1;
        }
        static public void ImaTryingFPN1()
        {

            uint flop1 = ((uint)3 << 23) + 4;
            uint flop2 = ((uint)2 << 23) + 2;//+ (uint)Math.Pow(2, 31);//(uint)(Math.Pow(2, 31) + 1);
            uint swapflop;
            uint flopresult = 0;
            uint tgexp = (uint)(Math.Pow(2, 31) - Math.Pow(2, 23));
            uint tgmant = (uint)(Math.Pow(2, 23) - 1);
            uint tgsign = (uint)(Math.Pow(2, 31));

            Console.WriteLine("flop1 is {0}\n{1}", ((flop1 & tgmant) * 0.1 + 1) * Math.Pow(10, ((flop1 & tgexp) >> 23)) * Math.Pow(-1, ((flop1 & tgsign) >> 31)), WriteInBinary(flop1));
            Console.WriteLine("flop2 is {0}\n{1}", ((flop2 & tgmant) * 0.1 + 1) * Math.Pow(10, ((flop2 & tgexp) >> 23)) * Math.Pow(-1, ((flop2 & tgsign) >> 31)), WriteInBinary(flop2));
            if ((flop1 & tgexp) == (flop2 & tgexp))
            {
                //!!!
            }
            // 
            else if (((flop1 & tgmant) * 0.1 + 1) * Math.Pow(10, ((flop1 & tgexp) >> 23)) < ((flop2 & tgmant) * 0.1 + 1) * Math.Pow(10, ((flop2 & tgexp) >> 23)))
            {
                swapflop = flop1;
                flop1 = flop2;
                flop2 = swapflop;
            }

            flopresult += flop1 & tgexp;
            int exp_dif = (int)(((flop1 & tgexp) >> 23) - ((flop2 & tgexp) >> 23));

            flop2 = ((flop2 & tgmant) << exp_dif) + (flop2 & tgexp) + (flop2 & tgsign);

            Console.WriteLine("flop1 is {0}\n{1}", ((flop1 & tgmant) * 0.1 + 1) * Math.Pow(10, ((flop1 & tgexp) >> 23)) * Math.Pow(-1, ((flop1 & tgsign) >> 31)), WriteInBinary(flop1));
            Console.WriteLine("flop2 is {0}\n{1}", ((flop2 & tgmant) * 0.1 + 1) * Math.Pow(10, ((flop2 & tgexp) >> 23)) * Math.Pow(-1, ((flop2 & tgsign) >> 31)), WriteInBinary(flop2));

            if ((flop1 & tgsign) == (flop2 & tgsign))
            {
                flopresult += (flop1 & tgmant) + (flop2 & tgmant) + (flop1 & tgsign);
            }
            else
            {
                flopresult += (flop1 & tgmant) - (flop2 & tgmant) - /*(uint)Math.Pow(10, ((flopresult & tgexp) >> 23) - 1)*/ +(flop1 & tgsign);
            }
            Console.WriteLine("{0}", ((flopresult & tgmant) * 0.1 + 1) * Math.Pow(10, ((flopresult & tgexp) >> 23)));
            Console.WriteLine("result:\n binary : {0}\ndecimal : {1}", WriteInBinary(flopresult),
                ((flopresult & tgmant) * 0.1 + 1) * Math.Pow(10, ((flopresult & tgexp) >> 23)));
            Console.ReadKey();

        }
        static public void TryingFlop2()
        {
            uint flop1 = 1102970880;
            uint flop2 = 1081081856;
            uint flopResult = 0;

            uint get_exponent = 2139095040;
            uint get_sign = 2147483648;
            uint get_mantice = 8388607;
            int exp_diff = 0;
            Console.WriteLine("Flop 1 is {0}\nFlop 2 is {1}", WriteInBinary(flop1), WriteInBinary(flop2));

            if (((flop1 << 1) >> 24) == ((flop2 << 1) >> 24))
            {

            }
            else if(true)
            {
                //here abs values are compared and switched if neccesary
            }

            flopResult = 0;
            flopResult += flop1 & get_exponent; //getting e3 exponent

            exp_diff = (int)(((flop1 & get_exponent) >> 23) - ((flop2 & get_exponent) >> 23));
            Console.WriteLine("Exp_difference is {0}", exp_diff);

            //flop2 = (flop2 & get_sign) + (flop2 & get_exponent) + (((flop2 & get_mantice) >> exp_diff));
            flop2 = (flop2 & get_sign) + (flop1 & get_exponent) + (((flop2 & get_mantice) >> exp_diff));
            //Console.WriteLine("Flop2, mantica moved left {0}", WriteInBinary(flop2));
            Console.WriteLine("Flop2, ... {0}", WriteInBinary(flop2));

            if ((flop1 & get_sign)==(flop2 & get_sign))
            {
                int sdvig = (int)((flop1 & get_exponent) >> 23) - 127;
                flopResult += (flop1 & get_sign) + (flop1 & get_mantice) + (flop2 & get_mantice);// + ( (uint)Math.Pow(2, 22) >> sdvig);
            }
            else
            {

            }             
            Console.WriteLine("Addition Result: {0}", Convert.ToString(flopResult, 2));
        }

        struct floatingpointnum{
            public byte sign;
            public int exponent;
            public int mantissa;
        }
        static public void TryingFlop3()
        {
            floatingpointnum fl1;
            floatingpointnum fl2;


            fl1.sign = 0;
            fl2.sign = 0;

            fl1.exponent = 2;
            //try
            //{
            //    int temp;
            //    Console.WriteLine("Enter first number.");
            //    temp = int.Parse(Console.ReadLine());
            //    //Console.WriteLine("{0}  --- {1}", temp[0], temp[31]);
            //    fl1.sign = temp >> 31;

            //}
            //catch
            //{
            //    return;
            //}
        }
        static public void FlPtBintoDec(uint flp)
        {
            uint get_exponent = 2139095040;
            uint get_sign = 2147483648;
            uint get_mantice = 8388607;

            Console.WriteLine("{0}", Math.Pow(-1, (flp & get_sign) >> 31) * Math.Pow(2, ((flp & get_exponent) >> 23) - 127) * ((1 << 23) + flp & get_mantice));
        }
        static void Main(string[] args)
        {
            //MultiplyAsIs();

            //Dividevariant3();

            TryingFlop3();
            //FlPtBintoDec(1);

            Console.ReadKey();
        }
    }
}
    